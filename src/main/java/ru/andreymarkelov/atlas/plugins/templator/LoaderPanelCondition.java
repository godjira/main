package ru.andreymarkelov.atlas.plugins.templator;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractIssueWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

public class LoaderPanelCondition extends AbstractIssueWebCondition {
    private final TemplateStoreDataService dataService;

    public LoaderPanelCondition(TemplateStoreDataService dataService) {
        this.dataService = dataService;
    }

    @Override
    public boolean shouldDisplay(ApplicationUser applicationUser, Issue issue, JiraHelper jiraHelper) {
        for (TemplateStoreData data : dataService.getTemplateStoreDataList()) {
            if (data != null
                   && data.getIssueTypeKey().equals(issue.getIssueTypeObject().getId())
                   && data.getProjectKey().equals(issue.getProjectObject().getKey())) {
                return true;
            }
        }
        return false;
    }
}
