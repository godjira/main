package ru.andreymarkelov.atlas.plugins.templator;

import java.util.Date;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.DateCFType;
import com.atlassian.jira.issue.customfields.impl.DateTimeCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.collect.MapBuilder;

public class TemplatorFieldRenderer {
    private JiraAuthenticationContext authenticationContext;
    private FieldScreenRendererFactory fieldScreenRendererFactory;
    private CustomFieldManager cfMgr;
    private Issue issue;
    private ExportType type;

    public TemplatorFieldRenderer(
            JiraAuthenticationContext authenticationContext,
            FieldScreenRendererFactory fieldScreenRendererFactory,
            CustomFieldManager cfMgr,
            Issue issue, 
            ExportType type) {
        this.authenticationContext = authenticationContext;
        this.fieldScreenRendererFactory = fieldScreenRendererFactory;
        this.cfMgr = cfMgr;
        this.issue = issue;
        this.type = type;
    }

    private String render(CustomField field, String defaultVal) {
        if (field == null) {
            return defaultVal;
        }

        FieldScreenRenderLayoutItem fl = fieldScreenRendererFactory.getFieldScreenRenderer(ApplicationUsers.toDirectoryUser(authenticationContext.getUser()), issue, IssueOperations.VIEW_ISSUE_OPERATION, true).getFieldScreenRenderLayoutItem(field);
        Map<String, Object> displayParams = MapBuilder.<String, Object>newBuilder("textOnly", Boolean.TRUE).add("nolink", Boolean.TRUE).toMutableMap();

        if (fl.getFieldLayoutItem().isHidden()) {
            return defaultVal;
        }

        Object value = issue.getCustomFieldValue(field);
        if (value == null) {
            return defaultVal;
        }

        if (field.getCustomFieldType().getClass().equals(DateCFType.class)) {
            DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
            DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_PICKER);
            return dtf.format((Date) value);
        } else if (field.getCustomFieldType().getClass().equals(DateTimeCFType.class)) {
            DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
            DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_TIME_PICKER);
            return dtf.format((Date) value);
        } else if (type == ExportType.DOCX){
            String values = issue.getCustomFieldValue(field).toString();
            String strWithOutAmpSymballs = values.replaceAll("&", "&amp;");
            if(strWithOutAmpSymballs.startsWith("[")){
                return strWithOutAmpSymballs.substring(1, values.length()-1); //remove [ ] 	
            }
            else {
                return strWithOutAmpSymballs;
            }
        } else{
            return field.getViewHtml(fl.getFieldLayoutItem(), null, issue, displayParams);
        }
    }

    public String renderField(String fieldName) {
        return renderField(fieldName, "");
    }

    public String renderField(String fieldName, String defaultVal) {
        return render(cfMgr.getCustomFieldObjectByName(fieldName), defaultVal);
    }

    public String renderFieldById(Number fieldId) {
        return renderFieldById(fieldId, "");
    }

    public String renderFieldById(Number fieldId, String defaultVal) {
        return render(cfMgr.getCustomFieldObject("customfield_".concat(fieldId.toString())), defaultVal);
    }

    public String renderFieldById(String fieldId) {
        return renderFieldById(fieldId, "");
    }

    public String renderFieldById(String fieldId, String defaultVal) {
        return render(cfMgr.getCustomFieldObject("customfield_".concat(fieldId.toString())), defaultVal);
    }
}
