package ru.andreymarkelov.atlas.plugins.templator;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.util.TextUtils;

public class TemplatorIssueRenderer {
    private Issue issue;

    public TemplatorIssueRenderer(Issue issue) {
        this.issue = issue;
    }

    public Set<String> getAffectedVersions() {
        Set<String> versions = new TreeSet<String>();
        for (Version version : issue.getAffectedVersions()) {
            versions.add(version.getName());
        }
        return versions;
    }

    public String getAssignee() {
        User user = issue.getAssigneeUser();
        if (user != null) {
            return user.getDisplayName();
        } else {
            return "";
        }
    }

    public Set<String> getComponents() {
        Set<String> comps = new TreeSet<String>();
        for (ProjectComponent comp : issue.getComponentObjects()) {
            comps.add(comp.getName());
        }
        return comps;
    }

    public String getCreated() {
        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_PICKER);
        return dtf.format(issue.getCreated());
    }

    public String getCurrentUser() {
        ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
        if (user != null) {
            return user.getDisplayName();
        } else {
            return "";
        }
    }

    public String getDescription(ExportType type) {
        if(ExportType.DOCX == type){
            return issue.getDescription();
        }
        return TextUtils.htmlEncode(issue.getDescription());
    }

    public String getDue() {
        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_PICKER);
        return dtf.format(issue.getDueDate());
    }

    public String getEnvironment(ExportType type) {
        if(ExportType.DOCX == type){
            return issue.getEnvironment();
        }
        return TextUtils.htmlEncode(issue.getEnvironment());
    }

    public Set<String> getFixVersions() {
        Set<String> versions = new TreeSet<String>();
        for (Version version : issue.getFixVersions()) {
            versions.add(version.getName());
        }
        return versions;
    }

    public Set<String> getLabels() {
        Set<String> labels = new TreeSet<String>();
        for (Label label : issue.getLabels()) {
            labels.add(label.getLabel());
        }
        return labels;
    }

    public String getNowDate() {
        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_PICKER);
        return dtf.format(new Date());
    }

    public String getNowTime() {
        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_TIME_PICKER);
        return dtf.format(new Date());
    }

    public String getReporter() {
        User user = issue.getReporterUser();
        if (user != null) {
            return user.getDisplayName();
        } else {
            return "";
        }
    }

    public String getSummary(ExportType type) {
        if(ExportType.DOCX == type){
            return issue.getSummary();
        }
        return TextUtils.htmlEncode(issue.getSummary());
    }

    public String getUpdated() {
        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        DateTimeFormatter dtf = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_PICKER);
        return dtf.format(issue.getUpdated());
    }
}
