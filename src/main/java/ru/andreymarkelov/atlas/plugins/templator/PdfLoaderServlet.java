package ru.andreymarkelov.atlas.plugins.templator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;

import javax.mail.internet.MimeUtility;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.LookAndFeelBean;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.issue.views.util.IssueViewUtil;
import com.atlassian.jira.issue.views.util.SearchRequestViewUtils;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliers;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

public class PdfLoaderServlet extends HttpServlet {
    private static final long serialVersionUID = 6988634250819927858L;

    private static Log log = LogFactory.getLog(PdfLoaderServlet.class);

    private final TemplateStoreDataService dataService;
    private final LoginUriProvider loginUriProvider;
    private final IssueManager issueMgr;
    private final TemplateRenderer templateRenderer;
    protected final JiraAuthenticationContext authenticationContext;
    protected final ApplicationProperties applicationProperties;
    protected final CommentManager commentManager;
    protected final FieldScreenRendererFactory fieldScreenRendererFactory;
    protected final IssueViewUtil issueViewUtil;
    protected final FieldVisibilityManager fieldVisibilityManager;
    protected final CustomFieldManager cfMgr;

    public PdfLoaderServlet(
            TemplateStoreDataService dataService,
            LoginUriProvider loginUriProvider,
            IssueManager issueMgr,
            TemplateRenderer templateRenderer,
            JiraAuthenticationContext authenticationContext,
            ApplicationProperties applicationProperties,
            CommentManager commentManager,
            FieldScreenRendererFactory fieldScreenRendererFactory,
            IssueViewUtil issueViewUtil,
            FieldVisibilityManager fieldVisibilityManager,
            CustomFieldManager cfMgr) {
        this.dataService = dataService;
        this.loginUriProvider = loginUriProvider;
        this.issueMgr = issueMgr;
        this.templateRenderer = templateRenderer;
        this.authenticationContext = authenticationContext;
        this.applicationProperties = applicationProperties;
        this.commentManager = commentManager;
        this.fieldScreenRendererFactory = fieldScreenRendererFactory;
        this.issueViewUtil = issueViewUtil;
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.cfMgr = cfMgr;
    }

    private void createDocumentFromWord(
            HttpServletRequest req,
            HttpServletResponse resp) throws ServletException, IOException {
        String issueId = req.getParameter("issueId");
        String id = req.getParameter("id");
        Issue issue = issueMgr.getIssueObject(issueId);
        TemplateStoreData data = dataService.getTemplateStoreData(Long.valueOf(id));
        File filePath = Utils.getTemplateFile(data);
        DocxManipulator.generateAndSendDocx(filePath, getContext(issue, ExportType.DOCX), resp, templateRenderer);
    }
   
   private void createPdfFromHtml(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

   	String issueId = req.getParameter("issueId");
       String id = req.getParameter("id");
       Issue issue = issueMgr.getIssueObject(issueId);
       TemplateStoreData data = dataService.getTemplateStoreData(Long.valueOf(id));
       File filePath = Utils.getTemplateFile(data);

       String htmlContent = templateRenderer.renderFragment(readFileAsString(filePath.getAbsolutePath()), getContext(issue, ExportType.PDF));
       resp.setContentType("application/pdf;charset=UTF-8");
       String user_agent = req.getHeader("user-agent");
       boolean isInternetExplorer = (user_agent.indexOf("MSIE") > -1);
       if (isInternetExplorer) {
           resp.setHeader("Content-disposition", "attachment; filename=\"" + URLEncoder.encode(data.getFileName(), "utf-8") + ".pdf\"");
       } else {
           resp.setHeader("Content-disposition", "attachment; filename=\"" + MimeUtility.encodeWord(data.getFileName()) + ".pdf\"");
       }

       Document document = new Document();
       try {
           PdfWriter writer = PdfWriter.getInstance(document, resp.getOutputStream());
           writer.setInitialLeading(12.5f);
           document.open();
           document.addCreationDate();
           CSSResolver cssResolver = new StyleAttrCSSResolver();
           XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider();
           fontProvider.registerDirectories();
           fontProvider.register("/fonts/comic.ttf");
           fontProvider.register("/fonts/DoulosSILCompact-R.ttf");
           fontProvider.register("/fonts/mctime.ttf");
           fontProvider.setUseUnicode(true);
           CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
           HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
           htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
           PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
           HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
           CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
           XMLWorker worker = new XMLWorker(css, true);
           XMLParser p = new XMLParser(worker);
           p.parse(new StringReader(htmlContent));
           document.close();
       } catch (Exception e) {
           log.error("PdfLoaderServlet::doGet - An error occurred", e);
           resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
       } finally {
           document.close();
       }
   }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JiraAuthenticationContext authCtx = ComponentAccessor.getJiraAuthenticationContext();
        if (authCtx.getUser() == null) {
            redirectToLogin(req, resp);
            return;
        }

        String id = req.getParameter("id");
        TemplateStoreData data = dataService.getTemplateStoreData(Long.valueOf(id));
        File filePath = Utils.getTemplateFile(data);
        if (filePath == null) {
            log.error("PdfLoaderServlet::doGet - File is not found");
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        String ext = FilenameUtils.getExtension(filePath.getPath());
        if(ext.equals("docx")) {
            createDocumentFromWord(req, resp);
        }
        else {
            createPdfFromHtml(req, resp);
        } 
   }

    private Map<String, Object> getContext(Issue issue, ExportType type) {
        ApplicationUser user = authenticationContext.getUser();
        LookAndFeelBean lookAndFeelBean = LookAndFeelBean.getInstance(applicationProperties);
        TemplatorIssueRenderer issueRenderer = new TemplatorIssueRenderer(issue);

        String commentsTextPlain = "";
        for(Comment comment: commentManager.getCommentsForUser(issue, user)){
            commentsTextPlain += comment.getAuthorFullName() + ": " + comment.getBody() + " | ";
        }
        if(!commentsTextPlain.isEmpty()){
            commentsTextPlain = "| " + commentsTextPlain;
        }

        Map<String, Object> ctx = JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);

        ctx.put("issue", issue);
        ctx.put("summary", issueRenderer.getSummary(type));
        ctx.put("status", issue.getStatusObject());
        ctx.put("description", issueRenderer.getDescription(type));
        ctx.put("assignee", issueRenderer.getAssignee());
        ctx.put("reporter", issueRenderer.getReporter());
        ctx.put("env", issueRenderer.getEnvironment(type));
        ctx.put("created", issueRenderer.getCreated());
        ctx.put("updated", issueRenderer.getUpdated());
        ctx.put("due", issueRenderer.getDue());
        ctx.put("nowDate", issueRenderer.getNowDate());
        ctx.put("nowTime", issueRenderer.getNowTime());
        ctx.put("labels", issueRenderer.getLabels());
        ctx.put("components", issueRenderer.getComponents());
        ctx.put("affectVersions", issueRenderer.getAffectedVersions());
        ctx.put("fixVersions", issueRenderer.getFixVersions());
        ctx.put("currentUser", issueRenderer.getCurrentUser());
        ctx.put("i18n", authenticationContext.getI18nHelper());
        ctx.put("fieldVisibility", fieldVisibilityManager);
        ctx.put("linkingEnabled", applicationProperties.getOption(APKeys.JIRA_OPTION_ISSUELINKING));
        ctx.put("linkCollection", issueViewUtil.getLinkCollection(issue, ApplicationUsers.toDirectoryUser(user)));
        ctx.put("fieldScreenRenderer", fieldScreenRendererFactory.getFieldScreenRenderer(ApplicationUsers.toDirectoryUser(user), issue, IssueOperations.VIEW_ISSUE_OPERATION, true));
        ctx.put("votingEnabled", applicationProperties.getOption(APKeys.JIRA_OPTION_VOTING));
        ctx.put("remoteUser", user);
        ctx.put("fieldRenderer", new TemplatorFieldRenderer(authenticationContext, fieldScreenRendererFactory, cfMgr, issue, type));
        ctx.put("stringUtils", new StringUtils());
        ctx.put("encoder", new JiraUrlCodec());
        ctx.put("comments", commentManager.getCommentsForUser(issue, user));
        ctx.put("linkColour", lookAndFeelBean.getTextLinkColour());
        ctx.put("linkAColour", lookAndFeelBean.getTextActiveLinkColour());
        ctx.put("generatedInfo", SearchRequestViewUtils.getGeneratedInfo(ApplicationUsers.toDirectoryUser(user)));
        ctx.put("textPlainComments", commentsTextPlain);
        return ctx;
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private String readFileAsString(String filePath) throws IOException {
        StringBuilder fileData = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8"));
            char[] buf = new char[1024];
            int numRead = 0;
            while((numRead=reader.read(buf)) != -1){
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
            }
        } finally {
            reader.close();
        }
        return fileData.toString();
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }
}
