package ru.andreymarkelov.atlas.plugins.templator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.sal.api.auth.LoginUriProvider;

public class TemplateLoaderServlet extends HttpServlet {
    private static final long serialVersionUID = -6404131971089536157L;

    private static Log log = LogFactory.getLog(PdfLoaderServlet.class);

    private final TemplateStoreDataService dataService;
    private final LoginUriProvider loginUriProvider;

    private File templateFile;
    private List<String> messages;
    private TemplateStoreData data;

    public TemplateLoaderServlet(TemplateStoreDataService dataService, LoginUriProvider loginUriProvider) {
        this.dataService = dataService;
        this.loginUriProvider = loginUriProvider;
        this.messages = new ArrayList<String>();
    }

    public void addMessage(String errorMessage) {
        this.messages.add(errorMessage);
    }

    public void clearMessages() {
        this.messages.clear();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JiraAuthenticationContext authCtx = ComponentAccessor.getJiraAuthenticationContext();
        if (authCtx.getUser() == null) {
            log.error("TemplateLoaderServlet::doGet - Not logged user");
            redirectToLogin(req, resp);
            return;
        }

        String id = req.getParameter("id");
        TemplateStoreData data = dataService.getTemplateStoreData(Long.valueOf(id));
        if (data == null) {
            log.error("TemplateLoaderServlet::doGet - Plugin data is not found");
            dataService.setTemplateStoreData(Long.valueOf(id), null);
            resp.sendRedirect(Utils.getBaseUrl(req).concat("/secure/AMTemplateConfigurer!default.jspa"));
            return;
        }

        File filePath = Utils.getTemplateFile(data);
        if (!filePath.exists()) {
            log.error("TemplateLoaderServlet::doGet - File doesn't exist");
            resp.sendRedirect(Utils.getBaseUrl(req).concat("/secure/AMTemplateConfigurer!default.jspa"));
            return;
        }

        resp.setContentType("text/plain");
        resp.setHeader("Content-disposition","attachment; filename=" + data.getTemplateName());
        OutputStream out = resp.getOutputStream();
        FileInputStream in = new FileInputStream(filePath);
        int size = in.available();
        byte[] content = new byte[size];
        in.read(content);
        out.write(content);
        in.close();
        out.flush();
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JiraAuthenticationContext authCtx = ComponentAccessor.getJiraAuthenticationContext();
        if (authCtx.getUser() == null) {
            log.error("TemplateLoaderServlet::doPost - Not logged user");
            redirectToLogin(req, resp);
            return;
        }

        init(req);

        if (!getMessages().isEmpty()) {
            req.getSession().setAttribute("AmTemplateErrorMessage", getMessages());
            req.getSession().setAttribute("AmTemplateData", data);
            if (getTemplateFile() != null) getTemplateFile().delete();
            resp.sendRedirect(Utils.getBaseUrl(req).concat("/secure/AMTemplateConfigurer!configure.jspa"));
        } else {
            req.getSession().removeAttribute("AmTemplateErrorMessage");
            req.getSession().removeAttribute("AmTemplateData");
            dataService.setTemplateStoreData(data.getId(), data);
            resp.sendRedirect(Utils.getBaseUrl(req).concat("/secure/AMTemplateConfigurer!default.jspa"));
        }
    }

    public List<String> getMessages() {
        return messages;
    }

    public File getTemplateFile() {
        return templateFile;
    }

    private String getText(String key, String...args) {
        return ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText(key, args);
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private void init(HttpServletRequest req) {
        clearMessages();
        data = new TemplateStoreData();

        ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
        List<FileItem> items = null;
        try {
            items = upload.parseRequest(req);
        } catch (FileUploadException e) {
            addMessage(getText("common"));
            return;
        }

        String hiddenid = null;
        String hiddenfile = null;
        Iterator<FileItem> iter = items.iterator();
        while (iter.hasNext()) {
            FileItem item = iter.next();
            if (item.isFormField()) {
                String value = item.getString();
                if (item.getFieldName().equals("projectkey")) {
                    if (value == null || value.length() == 0) {
                        addMessage(getText("ru.andreymarkelov.templator.admin.project.error"));
                    } else {
                        data.setProjectKey(value);
                    }
                } else if (item.getFieldName().equals("issuetype")) {
                    if (value == null || value.length() == 0) {
                        addMessage(getText("ru.andreymarkelov.templator.admin.issuetype.error"));
                    } else {
                        data.setIssueTypeKey(value);
                    }
                } else if (item.getFieldName().equals("statuses")) {
                    if (value != null && value.length() > 0) {
                        data.addStatus(value);
                    }
                } else if (item.getFieldName().equals("filename")) {
                    if (value == null || value.length() == 0) {
                        addMessage(getText("ru.andreymarkelov.templator.admin.filename.error"));
                    } else {
                        data.setFileName(Utils.getStringFromInputStream(item));
                    }
                } else if (item.getFieldName().equals("hiddenid")) {
                    if (value != null && value.length() > 0) {
                        hiddenid = value;
                    }
                } else if (item.getFieldName().equals("hiddenfile")) {
                    if (value != null && value.length() > 0) {
                        hiddenfile = value;
                    }
                }
            }
        }

        if (hiddenid != null) {
            data.setId(Long.valueOf(hiddenid));
            data.setTemplateName(hiddenfile);
            setTemplateFile(Utils.getTemplateFile(data));
        } else {
            data.setId(Counter.getValue());
        }
        data.setUpdateTime(new Date().getTime());

        iter = items.iterator();
        while (iter.hasNext()) {
            FileItem item = iter.next();
            if (!item.isFormField()) {
                File file = new File(Utils.getTemplateDir(data.getId()), item.getName());
                if (item.getSize() > 0) {
                    try {
                        item.write(file);
                        setTemplateFile(file);
                        data.setTemplateName(file.getName());
                    } catch (Exception e) {
                        addMessage(getText("ru.andreymarkelov.templator.admin.template.error", e.getMessage()));
                    }
                }
            }
        }

        if (data.getStatuses().isEmpty()) {
            addMessage(getText("ru.andreymarkelov.templator.admin.statuses.error"));
            return;
        }

        if (getTemplateFile() == null) {
            addMessage(getText("ru.andreymarkelov.templator.admin.template.empty"));
        }
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    public void setTemplateFile(File templateFile) {
        this.templateFile = templateFile;
    }
}
