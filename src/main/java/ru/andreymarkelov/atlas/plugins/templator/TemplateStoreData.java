package ru.andreymarkelov.atlas.plugins.templator;

import java.util.ArrayList;
import java.util.List;

public class TemplateStoreData implements Comparable<TemplateStoreData> {
    private Long id;
    private String projectKey;
    private String issueTypeKey;
    private List<String> statuses;
    private String fileName;
    private String templateName;
    private long updateTime;

    public TemplateStoreData() {
        this.statuses = new ArrayList<String>();
    }

    public void addStatus(String status) {
        statuses.add(status);
    }

    @Override
    public int compareTo(TemplateStoreData o) {
        return Long.valueOf(Long.valueOf(o.getUpdateTime())).compareTo(updateTime);
    }

    public String getFileName() {
        return fileName;
    }

    public Long getId() {
        return id;
    }

    public String getIssueTypeKey() {
        return issueTypeKey;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public List<String> getStatuses() {
        return statuses;
    }

    public String getTemplateName() {
        return templateName;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIssueTypeKey(String issueTypeKey) {
        this.issueTypeKey = issueTypeKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TemplateStoreData [id=" + id + ", projectKey=" + projectKey
            + ", issueTypeKey=" + issueTypeKey + ", statuses=" + statuses
            + ", fileName=" + fileName + ", templateName=" + templateName
            + ", updateTime=" + updateTime + "]";
    }
}
