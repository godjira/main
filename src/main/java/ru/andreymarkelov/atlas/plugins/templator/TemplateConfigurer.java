package ru.andreymarkelov.atlas.plugins.templator;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class TemplateConfigurer extends JiraWebActionSupport {
    private static final long serialVersionUID = -7644234516648078481L;

    private final TemplateStoreDataService dataService;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectManager projectManager;
    private final IssueTypeManager issueTypeManager;
    private final SubTaskManager subTaskManager;
    private final StatusManager statusManager;

    private List<TemplateStoreData> datas;
    private TemplateStoreData data;
    private String id;

    public TemplateConfigurer(
            TemplateStoreDataService dataService,
            JiraAuthenticationContext authenticationContext,
            ProjectManager projectManager,
            IssueTypeManager issueTypeManager,
            SubTaskManager subTaskManager,
            StatusManager statusManager) {
        this.dataService = dataService;
        this.authenticationContext = authenticationContext;
        this.projectManager = projectManager;
        this.issueTypeManager = issueTypeManager;
        this.subTaskManager = subTaskManager;
        this.statusManager = statusManager;
    }

    public String doConfigure() {
        if (!hasAdminPermission()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        if (getId() != null) {
            data = dataService.getTemplateStoreData(Long.valueOf(getId()));
        }

        Object AmTemplateErrorMessage = getHttpRequest().getSession().getAttribute("AmTemplateErrorMessage");
        if (AmTemplateErrorMessage != null) {
            @SuppressWarnings("unchecked")
            List<String> errors = (List<String>) AmTemplateErrorMessage;
            addErrorMessages(errors);
        }
        Object AmTemplateData = getHttpRequest().getSession().getAttribute("AmTemplateData");
        if (AmTemplateData != null) {
            TemplateStoreData tempData = (TemplateStoreData) AmTemplateData;
            if (data != null) {
                tempData.setId(data.getId());
            } else {
                tempData.setId(null);
            }
            data = tempData;
        }
        getHttpRequest().getSession().removeAttribute("AmTemplateErrorMessage");
        getHttpRequest().getSession().removeAttribute("AmTemplateData");

        return "configure";
    }

    @Override
    public String doDefault() throws Exception {
        if (!hasAdminPermission()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        datas = dataService.getTemplateStoreDataList();
        return INPUT;
    }

    @Override
    protected String doExecute() throws Exception {
        if (getId() != null) {
            dataService.setTemplateStoreData(Long.valueOf(getId()), null);
        }
        return getRedirect("AMTemplateConfigurer!default.jspa");
    }

    public String getBackLink() {
        return getBaseUrl().concat("/secure/AMTemplateConfigurer!default.jspa");
    }

    public String getBaseUrl() {
        return Utils.getBaseUrl(getHttpRequest());
    }

    public TemplateStoreData getData() {
        return data;
    }

    public List<TemplateStoreData> getDatas() {
        if (datas != null) {
            Collections.sort(datas);
        }
        return datas;
    }

    public String getId() {
        return id;
    }

    public Map<String, String> getIssueTypes() {
        Map<String, String> map = new TreeMap<String, String>();
        for (IssueType issueType : issueTypeManager.getIssueTypes()) {
            map.put(issueType.getId(), "Issue type - " + issueType.getNameTranslation());
        }
        for (IssueType issueType : subTaskManager.getSubTaskIssueTypeObjects()) {
            map.put(issueType.getId(), "Subtask - " + issueType.getNameTranslation());
        }
        return Utils.sortByValues(map);
    }

    public Map<String, String> getProjects() {
        Map<String, String> map = new TreeMap<String, String>();
        for (Project project : projectManager.getProjectObjects()) {
            map.put(project.getKey(), project.getName());
        }
        return Utils.sortByValues(map);
    }

    public Map<String, String> getStatuses() {
        Map<String, String> map = new TreeMap<String, String>();
        for (Status status : statusManager.getStatuses()) {
            map.put(status.getId(), status.getNameTranslation());
        }
        return Utils.sortByValues(map);
    }

    public boolean hasAdminPermission() {
        ApplicationUser user = authenticationContext.getUser();
        if (user == null) {
            return false;
        }

        if (!ComponentAccessor.getPermissionManager().hasPermission(Permissions.ADMINISTER, user)) {
            return false;
        }

        return true;
    }

    public String renderDate(long date) {
        if (date <= 0) {
            return "";
        } else {
            DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
            return dateTimeFormatterFactory.formatter().forLoggedInUser().format(new Date(date));
        }
    }

    public String renderIssueType(String itId) {
        IssueType it = issueTypeManager.getIssueType(itId);
        if (it != null) {
            return it.getNameTranslation();
        } else {
            IssueType subit = subTaskManager.getSubTaskIssueType(itId);
            if (subit != null) {
                return subit.getNameTranslation();
            } else {
                return itId;
            }
        }
    }

    public String renderProject(String projectKey) {
        Project proj = projectManager.getProjectObjByKey(projectKey);
        if (proj != null) {
            return proj.getName();
        } else {
            return projectKey;
        }
    }

    public String renderStatus(String statusId) {
        Status status = statusManager.getStatus(statusId);
        return (status != null) ? status.getNameTranslation() : statusId;
    }

    public void setId(String id) {
        this.id = id;
    }
}
