package ru.andreymarkelov.atlas.plugins.templator;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.util.TextUtils;

@Path("/templator")
public class TemplatorSrv {
    private static Log log = LogFactory.getLog(TemplatorSrv.class);

    private final TemplateStoreDataService dataService;
    private final IssueManager issueMgr;
    private final JiraAuthenticationContext authCtx;

    public TemplatorSrv(TemplateStoreDataService dataService, IssueManager issueMgr, JiraAuthenticationContext authCtx) {
        this.dataService = dataService;
        this.issueMgr = issueMgr;
        this.authCtx = authCtx;
    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    @Path("/check")
    public Response checkIssueStatus(@Context HttpServletRequest req) {
        if (ComponentAccessor.getJiraAuthenticationContext().getUser() == null) {
            log.error("TemplatorSrv::checkIssueStatus - user is not logged");
            return makeBoolResponse(null, null, req);
        }

        String issueId = req.getParameter("issueId");
        if (!StringUtils.isEmpty(issueId)) {
            Issue issue = issueMgr.getIssueObject(issueId);
            if (issue != null) {
                return makeBoolResponse(checkStatus(issue), issue, req);
            }
        }
        return makeBoolResponse(null, null, req);
    }

    private List<TemplateStoreData> checkStatus(Issue issue) {
        List<TemplateStoreData> res = new ArrayList<TemplateStoreData>();
        for (TemplateStoreData data : dataService.getTemplateStoreDataList()) {
            if (data != null
                   && data.getStatuses().contains(issue.getStatusObject().getId())
                   && data.getProjectKey().equals(issue.getProjectObject().getKey())
                   && data.getIssueTypeKey().equals(issue.getIssueTypeObject().getId())) {
                res.add(data);
            }
        }
        return res;
    }

    private String getBody(List<TemplateStoreData> list, Issue issue, HttpServletRequest req) {
        if (list != null && !list.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("<div class='aui-message generic'><span class='aui-icon icon-generic'/>");
            for (TemplateStoreData data : list) {
                String href = Utils.getBaseUrl(req) + "/plugins/servlet/andreymarkelov/pdfloader?id=" + data.getId() + "&issueId=" + issue.getKey();
                sb.append("<p><strong>");
                sb.append("<a class='donwloadlink' href=" + TextUtils.htmlEncode(href) + ">" + authCtx.getI18nHelper().getText("ru.andreymarkelov.templator.panel.download") + ": " + TextUtils.htmlEncode(data.getFileName()) + "</a>");
                sb.append("</strong></p>");
            }
            sb.append("</div>");
            return sb.toString();
        } else {
            return "<div class='aui-message info'><span class='aui-icon icon-info'/><strong>" + authCtx.getI18nHelper().getText("ru.andreymarkelov.templator.panel.notstatus") + "</strong></div>";
        }
    }

    private Response makeBoolResponse(List<TemplateStoreData> list, Issue issue, HttpServletRequest req) {
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);
        cc.setNoStore(true);
        cc.setMaxAge(0);
        return Response.ok(new HtmlEntity(getBody(list, issue, req))).type(MediaType.APPLICATION_JSON_TYPE).cacheControl(cc).build();
    }
}
