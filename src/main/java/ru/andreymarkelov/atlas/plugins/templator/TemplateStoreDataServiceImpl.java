package ru.andreymarkelov.atlas.plugins.templator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class TemplateStoreDataServiceImpl implements TemplateStoreDataService {
    private final static String TEMPLATE_LOADER_KEY = "andreymarkelov-atlas-plugins.templator";
    private final static String TEMPLATE_LOADER_TEMPLATES = "templates";

    private final PluginSettings pluginSettings;

    private Object lock = new Object();

    public TemplateStoreDataServiceImpl(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createSettingsForKey(TEMPLATE_LOADER_KEY);
    }

    private Set<Long> getTemplates() {
        return Utils.strToListLongs((String) pluginSettings.get(TEMPLATE_LOADER_TEMPLATES));
    }

    @Override
    public TemplateStoreData getTemplateStoreData(Long id) {
        synchronized(lock) {
            Object value = getValue(id.toString());
            if (value != null) {
                return TemplateStoreDataTranlator.fromString(value.toString());
            }
            return null;
        }
    }

    @Override
    public List<TemplateStoreData> getTemplateStoreDataList() {
        synchronized(lock) {
            List<TemplateStoreData> res = new ArrayList<TemplateStoreData>();
            for (Long id : getTemplates()) {
                TemplateStoreData data = getTemplateStoreData(id);
                if (data != null) {
                    res.add(getTemplateStoreData(id));
                }
            }
            return res;
        }
    }

    private Object getValue(String key) {
        return pluginSettings.get(key);
    }

    private void saveTemplates(Set<Long> templates) {
        pluginSettings.put(TEMPLATE_LOADER_TEMPLATES, Utils.listLongsToStr(templates));
    }

    @Override
    public void setTemplateStoreData(Long id, TemplateStoreData data) {
        synchronized(lock) {
            Set<Long> templates = getTemplates();
            if (data == null) {
                templates.remove(id);
                setValue(id.toString(), null);
            } else {
                templates.add(data.getId());
                setValue(id.toString(), TemplateStoreDataTranlator.toString(data));
            }
            saveTemplates(templates);
        }
    }

    private void setValue(String key, String data) {
        if (data == null) {
            pluginSettings.remove(key);
        } else {
            pluginSettings.put(key, data);
        }
    }
}
